### Konfiguracja

#### Narzędzia i ustawienia ####

Cały warsztat prowadzony jest przy wykorzystaniu poniższego środowiska, możesz użyć tego samego, lub twój ulubiony edytor codu (IDE) i przeglądarkę, ja preferują Chrome'a i VSCode ;)

Edytor kodu: Visual Studio Code, ściągnij go [tutaj](http://code.visualstudio.com/) dla Maca, Windowsa and Linuxa.

- (VSCode Plugin) Angular Language features: [Link](https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline)

- (VSCode Plugin) VSCode Icons: [Link](https://marketplace.visualstudio.com/items?itemName=robertohuertasm.vscode-icons)

Przeglądarka: Google Chrome, możesz ściągnać ją [tutaj](https://www.google.com/chrome).

##### Wymagania wstępne #####

Please make sure that you have the following installed:

Zainstaluj najnowszą wersję [Node.js](http://nodejs.org/) (na Maca lub  Windows)
Użytkownicy Maczka mogą opcjonalnie użyć komendy ```brew install node``` jeśli mają zainstalowany brew
Będzie potrzebny Node Sass, jeśli nie masz go jeszcze zainstalowanego:

```npm install -g node-sass```

#### Instalacja projektu ####

Aby ściągnąć projekt startowy, ściągnij go w postaci zipa.

##### Krok 1: Manager Pakietów #####

Aby zainstalować wszystkie zależności, musisz zainstalować Yarn. Jeśli masz zainstalowany Yarn,miej pewność, że masz najnowszą wersję, jeśli jej nie masz zrób upgrade. W celu instalacji Yarna otwórz terminal i wpradź następującą komendę:

```npm install -g yarn```

Użytkownicy Maca mogą alternatywnie użyć brew by zainstalować yarna.

```brew update```

```brew install yarn```

Jeśli masz problemy z instalacją i używaniem yarna możesz więcej dowiedzieć się [tutaj](https://yarnpkg.com/en/docs/install).

##### Krok 2: Pakiety do Projektu #####

Mamy już manager pakierów, zatem możemy zainstalować wszystkie zależności. Możesz to zrobić przez wprowadzenie komendy w terminalu:

```yarn install```

##### Krok 3: Odpalenie projektu #####

W procesie developmentu, projekt jest budowany przy użyciu webpack-dev-server. To pozwala na debugowanie i testowanie kodu na środowisku lokalny oraz recompiluje cały projekt gdy zmieni się jakiś plik w źródłach. Przęgladarka także sama się odświeży gdy zmienimy coś w kodzie.

Aby odpalić projekt, wprowadź w terminalu następującą komendę:

```yarn start```

Otwórz przeglądarkę [localhost:4000](http://localhost:4000) by wystartować projekt.

Jeśli widzicie tekst Hello w przeglądarce to wszystko gotowe :+1:

#### Kompilacja projektu ####

Projekt używa webpacka by zbudować i skompilować przystkie zasoby:

- Skompiluje wszystkie pliki kodu w TypeScript'cie do JavaScript'a (startując main.ts i po kolei kolejne pliki według importów)
- Scali wszystkie pliki JavaScript do jednego pliku
- Pozwali na użycie Sass do stylizacji naszych komponentów
- Dostarczy wszystkie polyfille potrzebne, aby nasza apka działała prawidłowo na wszystkich najnowszych przeglądarkach
- Zmockuje JSON'owy backend przy użyciu json-server